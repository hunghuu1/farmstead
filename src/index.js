function ScrollToElement(idmenu,idclick){

    if(typeof idmenu === "string" && typeof idclick === "string"){

        let menu = document.querySelector(idmenu);
        let click = document.querySelector(idclick);

        if(menu&&click){
            menu.addEventListener('click', function(){
                click.scrollIntoView({
                    behavior: "smooth", 
                    block: "start", 
                    inline: "nearest",
                });
            })
        }
    }
}

ScrollToElement('#click','#menu')

function menuMobile (buttonNav,Menu){
    if(typeof buttonNav === 'string' && typeof Menu === 'string'){
        // Check du lieu truyen vao ham la chuoi thi moi chay
        let btnMenu = document.querySelector(buttonNav);
        let NavMenu = document.querySelector(Menu);
        let html = document.querySelector('html')
        // Kiem tra element co ton tai hay k
        if(btnMenu && NavMenu){
            // Khi button click se scroll toi element da chon
            btnMenu.addEventListener('click',function(){
                NavMenu.classList.toggle('active')
                html.classList.toggle('active')
            });
        }
    }
}
menuMobile('#about','#scroll')